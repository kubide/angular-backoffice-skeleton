import {Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'shared/services/auth/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { PageScrollService, PageScrollInstance, PageScrollConfig } from 'ng2-page-scroll';
import { DOCUMENT } from '@angular/common';
import { ErrorsService, Error } from 'shared/services/errors/errors.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { Silly, SillyService } from 'shared/services/silly/silly.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private subErrors: Subscription;
  private errors$: Observable<Error[]>;
  public toasterconfig: ToasterConfig;


  constructor(
              private router: Router,
              private toasterService: ToasterService,
              private errorsService: ErrorsService,
              private pageScrollService: PageScrollService,
              @Inject(DOCUMENT) private document: any) {

  }

  ngOnInit() {

    const options = {
      document: this.document,
      scrollTarget: 'body',
      pageScrollOffset: 0,
      pageScrollDuration: 0
    };
    const pageScrollInstance: PageScrollInstance = PageScrollInstance.newInstance(options);

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(() => {
        this.pageScrollService.start(pageScrollInstance);
      });

    this.toasterconfig = new ToasterConfig({
        showCloseButton: true,
        tapToDismiss: true,
        timeout: 3000,
      });

    this.errors$ = this.errorsService.getObservable();
    this.subErrors = this.errors$.subscribe((errors) => {
      if (!errors.length) {
        return false;
      }

      const show = errors[0].payload && errors[0].payload.message ? errors[0].payload.message : `Section ${errors[0].section}`;
      this.toasterService.pop('error', 'Error no capturado', show);
      console.log(errors[0]);

      this.errorsService.remove(errors[0].hash);
    });
  }

  ngOnDestroy() {
    this.subErrors.unsubscribe();
  }

}
