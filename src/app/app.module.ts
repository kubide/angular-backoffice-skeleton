import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Libraries
import { ToasterModule } from 'angular2-toaster';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { LaddaModule } from 'angular2-ladda';

// Services modules
import { LocalStorageModule } from 'shared/services/localStorage/localStorage.module';
import { SillyModule } from 'shared/services/silly/silly.module';
import { AuthModule } from 'shared/services/auth/auth.module';
import { ErrorsModule } from 'shared/services/errors/errors.module';
import { UsersServiceModule } from 'shared/services/users/users.module';
import { AuthGuard } from 'shared/services/auth/auth.guard';

// Components
import { AppComponent } from './app.component';
import { Page404Component } from './modules/page404/page404.component';
import { LogoutComponent } from './components/logout/logout.component';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: './modules/login/login.module#LoginModule'
  },
  { path: 'logout', component: LogoutComponent },
  {
    path: '',
    loadChildren: './modules/admin/admin.module#AdminModule',
    canActivateChild: [AuthGuard],
  },
  { path: '**', component: Page404Component }

];

@NgModule({
  declarations: [
    AppComponent,
    LogoutComponent,
    Page404Component
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    Ng2PageScrollModule,
    ToasterModule,
    BrowserAnimationsModule,
    LaddaModule.forRoot({
      style: 'zoom-in',
      spinnerSize: 40,
      spinnerColor: 'white',
      spinnerLines: 12,
    }),
    NgbModule.forRoot(),
    LocalStorageModule.forRoot(),
    SillyModule.forRoot(),
    ErrorsModule.forRoot(),
    AuthModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
