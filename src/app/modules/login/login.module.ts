import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent} from './login.component';
import { LocalStorageModule } from 'shared/services/localStorage/localStorage.module';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthModule } from 'shared/services/auth/auth.module';
import { ErrorsModule } from 'shared/services/errors/errors.module';

const ROUTES: Routes = [{
  path: '',
  data: {
    title: 'Login',
    urls: [ {title: 'Login', url: '/'}]
  },
  component: LoginComponent
  },
  {
    path: 'forgot-pass',
    loadChildren: './modules/forgotPass/forgotPass.module#ForgotPassModule'
  }
];

@NgModule({
  imports: [
    FormsModule,
    AuthModule,
    ReactiveFormsModule,
    CommonModule,
    LocalStorageModule,
    ErrorsModule,
    NgbModule.forRoot(),
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    LoginComponent
  ],
  providers: [
  ]

})
export class LoginModule { }
