import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPassComponent } from './forgotPass.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Forgot Pass'
  },
  component: ForgotPassComponent
}];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [
    ForgotPassComponent
  ]
})
export class ForgotPassModule { }
