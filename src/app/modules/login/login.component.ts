import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {ISubscription, Subscription} from 'rxjs/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from 'shared/services/auth/auth.service';
import { Credentials } from 'shared/interfaces/tokenAndCredentials.interface';
import {ErrorsService, Error} from 'shared/services/errors/errors.service';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-login-component',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']

})
export class LoginComponent implements OnInit, OnDestroy {
  auth$: Observable<any>;
  sub: ISubscription;
  loginForm: FormGroup;
  isSubmitLoginForm: boolean;
  isSetLogin: boolean;
  isSettingLogin: boolean;
  errorLogin: string;

  private subErrors: Subscription;
  private errors$: Observable<Error[]>;

  constructor(private router: Router,
              private authService: AuthService,
              private errorsService: ErrorsService,
              private toasterService: ToasterService,
              private fb: FormBuilder) {
    this.errorLogin = '';
    this.isSetLogin = false;
    this.isSettingLogin = false;
    this.isSubmitLoginForm = false;
    this.loginForm = fb.group({
      userName: [null, Validators.compose( [ Validators.required ] )],
      pass: [null, Validators.compose( [ Validators.required ] )],
      remember: []
    })
  }

  ngOnInit() {
    this.auth$ = this.authService.getObservable();
    this.sub = this.auth$.subscribe((user) => {
      if (user.hash) {

        this.router.navigate(['/']);
      }
      if (this.isSettingLogin) {
        this.isSetLogin = true;
        this.isSettingLogin = false;
      }
    });

    this.errors$ = this.errorsService.getObservable('errors.auth');
    this.subErrors = this.errors$.subscribe((errors) => {
      if (!errors.length) {
        return false;
      }
      this.errorLogin = errors[0].section;
      this.errorsService.remove(errors[0].hash);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onSubmit(form) {
    if (!form.valid) {
      this.isSubmitLoginForm = true;
      return;
    }

    const credentials = <Credentials>{
      grant_type: 'password',
      username: form.value.userName,
      password: form.value.pass,
    };
    this.isSettingLogin = true;
    console.log(credentials);
    this.authService.login(credentials);
  }
}
