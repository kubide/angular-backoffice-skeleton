import { merge } from 'lodash';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

// Interfaces
import { Options } from 'shared/services/abstract.service';
import { User } from 'shared/interfaces/user.interface';

// Services
import { UsersService } from 'shared/services/users/users.service';

@Component({
  selector: 'app-dashboards-list-container',
  templateUrl: './dashboards-list.component.html',
  styleUrls: ['./dashboards-list.component.css']
})
export class DashboardsListComponent implements OnInit {
  public users$: Observable<User[]>;
  public usersHeaders$: Observable<Headers>;
  public usersUpdating$: Observable<boolean>;
  private getOptions: Options = {};
  sortField: string;
  sortOrder: boolean;

  constructor(private userService: UsersService) {}

  ngOnInit() {
    this.users$ = this.userService.getObservable('standard');
    this.usersHeaders$ = this.userService.getObservable('standard', 'header');
    this.usersUpdating$ = this.userService.getObservable('standard', 'updating');

    this.getDataUser();
  }

  getDataUser() {
    const defaultOptions = <Options>{
      page: 1,
      limit: 10,
      fs: null,
      sort: '-createdAt',
    };
    const options = merge({}, defaultOptions, this.getOptions);
    this.userService.getUsers('standard', options);
  }

  newPage(event) {
    if (event) {
      this.getOptions.page = event;
      this.getDataUser();
    }
  }

  fieldSort(field) {
    this.sortOrder = !this.sortOrder;
    this.sortField = field;

    const direction = this.sortOrder ? '' : '-';

    this.getOptions.sort = direction.concat(this.sortField);
    this.getOptions.page = 1;
    this.getDataUser();
  }
}
