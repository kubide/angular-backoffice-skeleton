import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardsListComponent} from './dashboards-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersServiceModule } from 'shared/services/users/users.module';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from 'shared/services/auth/auth.http.interceptor';
import { ChartModule } from 'shared/components/chart/chart.module';

const routes: Routes = [{
    path: '',
    component: DashboardsListComponent
  }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ChartModule,
    UsersServiceModule.forRoot(),
    NgbModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardsListComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    }
  ],
})
export class DashboardsListModule {
}
