import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent} from './users-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersServiceModule } from 'shared/services/users/users.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from 'shared/services/auth/auth.http.interceptor';
import { CsvServiceModule } from 'shared/services/csv/csv.module';

const routes: Routes = [{
    path: '',
    component: UsersListComponent
  },
  {
    path: 'new',
    loadChildren: './modules/create-user/create-user.module#CreateUserModule'
  },
  // {
  //   path: ':userHash',
  //   loadChildren: './modules/modify-user/modify-user.module#ModifyUserModule'
  // }
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    CsvServiceModule.forRoot(),
    UsersServiceModule.forRoot(),
    NgbModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    UsersListComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    }
  ],
})
export class UsersListModule {
}
