import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUserComponent } from './create-user.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersServiceModule } from 'shared/services/users/users.module';
import { LaddaModule } from 'angular2-ladda';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from 'shared/services/auth/auth.http.interceptor';
import { ShowUserDataComponent } from '../../components/show-user-data/show-user-data.component';


const routes: Routes = [{
  path: '',
  component: CreateUserComponent
}];



@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UsersServiceModule,
    LaddaModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [
    CreateUserComponent,
    ShowUserDataComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    }
  ]
})
export class CreateUserModule { }
