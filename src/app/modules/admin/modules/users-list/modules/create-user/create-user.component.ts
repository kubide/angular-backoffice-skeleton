import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsersService } from 'shared/services/users/users.service';
import { Observable } from 'rxjs/Observable';
import { User } from 'shared/interfaces/user.interface';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import 'rxjs/add/operator/filter';
import { Location } from '@angular/common';
import { ToasterService } from 'angular2-toaster';
import { GenericAbstract } from 'shared/utils/abstract/generic.abstract.class';

@Component({
  selector: 'app-create-user-container',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']

})
export class CreateUserComponent extends GenericAbstract implements OnInit, OnDestroy {
  usersUpdating$: Observable<boolean>;
  sub: Subscription;
  user: User = <User>{};
  error: string;
  createForm: FormGroup;

  constructor(private usersService: UsersService,
              private router: Router,
              private toasterService: ToasterService,
              public _location: Location,
              private fb: FormBuilder) {
    super(_location);
  }

  ngOnInit() {
    this.generateForm();

    this.usersService.cleanSection('new-user');
    this.usersUpdating$ = this.usersService.getObservable('new-user', 'updating');
    this.sub = this.usersService.getObservable('new-user')
      .filter (users => !! users.length )
      .map(users => users[0])
      .subscribe((user) => {
        //this.router.navigate(['/users', user.hash]);
      });
  }

  /**
   * Form for create user
   */
  generateForm() {
    this.createForm = this.fb.group({
      name: ['', Validators.compose( [ Validators.required ] )],
      surname: ['', Validators.compose( [ Validators.required ] )],
      birthdate: ['', Validators.compose( [ Validators.required ] )],
      email: ['', Validators.compose( [ Validators.required ] )],
      phone: ['', Validators.compose( [ Validators.required ] )],
      fax: ['', Validators.compose( [ Validators.required ] )],
      company_name: ['', Validators.compose( [ Validators.required ] )],
      password: ['', Validators.compose( [ Validators.required ] )],
      confirm_password: ['', Validators.compose( [ Validators.required ] )],
      // Todo: show subgroup
      // shipping address
      address_shipping: ['', Validators.compose( [ Validators.required ] )],
      postal_code_shipping: ['', Validators.compose( [ Validators.required ] )],
      city_shipping: ['', Validators.compose( [ Validators.required ] )],
      country_shipping: ['', Validators.compose( [ Validators.required ] )],
      // Billing address
      address_billing: ['', Validators.compose( [ Validators.required ] )],
      postal_code_billing: ['', Validators.compose( [ Validators.required ] )],
      city_billing: ['', Validators.compose( [ Validators.required ] )],
      country_billing: ['', Validators.compose( [ Validators.required ] )],
    });
  }

  onSubmit() {
    if (!this.createForm.valid) {
      return;
      // Todo: show message: review form data
    }


    this.user = {
      name: this.createForm.value.name,
      surname: this.createForm.value.surname,
      birthdate: this.createForm.value.birthdate,
      email: this.createForm.value.email,
      phone: this.createForm.value.phone,
      fax: this.createForm.value.fax,
      companyName: this.createForm.value.company_name,
      password: this.createForm.value.password,
      shippingAddress: {
        address: this.createForm.value.address_shipping,
        postalCode: this.createForm.value.postal_code_shipping,
        city: this.createForm.value.city_shipping,
        country: this.createForm.value.country_shipping,
      },
      billingAddress: {
        address: this.createForm.value.address_billing,
        postalCode: this.createForm.value.postal_code_billing,
        city: this.createForm.value.city_billing,
        country: this.createForm.value.country_billing,
      }
    };

    debugger;
    //
    // // Check password
    // if (this.formGroup.value.password && this.formGroup.value.confirmPassword) {
    //   if (this.formGroup.value.password === this.formGroup.value.confirmPassword) {
    //     this.user.password = this.formGroup.value.password;
    //     this.user.confirmPassword = this.formGroup.value.confirmPassword;
    //   } else {
    //     this.error = 'Passwords do not match';
    //     return;
    //   }
    // }

     this.usersService.createUser('new-user', this.user);

    // Active notification
    this.toasterService.pop('success', 'Success', 'User created correctly.');
  }

  showPass() {
    document.getElementById('password')['type'] = document.getElementById('password')['type'] === 'password' ? 'text' : 'password';
    document.getElementById('confirmPassword')['type'] = document.getElementById('confirmPassword')['type'] === 'password' ? 'text' : 'password';
  }
  /**
   * END: Form for create user
   */

  ngOnDestroy() {
    try {
      this.sub.unsubscribe();
    } catch (e) { console.log('error', e); }
  }
}
