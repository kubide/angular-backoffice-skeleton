import { Component, Input } from '@angular/core';
import { User } from 'shared/interfaces/user.interface';

@Component({
  selector: 'app-show-user-data',
  templateUrl: './show-user-data.component.html',
  styleUrls: ['./show-user-data.component.scss']
})
export class ShowUserDataComponent {
  @Input() data: User;
}
