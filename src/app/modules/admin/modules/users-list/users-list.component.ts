import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { UsersService } from 'shared/services/users/users.service';
import { Observable } from 'rxjs/Observable';
import { User} from 'shared/interfaces/user.interface';
import { merge } from 'lodash';
import { Options } from 'shared/services/abstract.service';
import { ToasterService } from 'angular2-toaster';
import { CsvService } from 'shared/services/csv/csv.service';
import { ListAbstract } from 'shared/utils/abstract/list.abstract.class';

@Component({
  selector: 'app-users-list-container',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent extends ListAbstract implements OnInit, OnDestroy {
  dataCsv: string;
  public users$: Observable<User[]>;
  public usersHeaders$: Observable<Headers>;
  public usersUpdating$: Observable<boolean>;
  public csv$: Observable<any>;
  csv: any;

  constructor(private userService: UsersService,
              public csvService: CsvService,
              public toasterService: ToasterService) {
    super(csvService, toasterService);
  }

  ngOnInit() {
    this.users$ = this.userService.getObservable('standard');
    this.usersHeaders$ = this.userService.getObservable('standard', 'header');
    this.usersUpdating$ = this.userService.getObservable('standard', 'updating');

    this.csv$ = this.userService.getObservable('csv');

    this.userService.getObservable('csv')
      .filter (csv => !!csv.length)
      .map(csv => csv[0])
      .subscribe((csv) => {
        this.dataCsv = csv;
      });

    this.userService.getCsv('csv', {'export': 'csv-new'});
    this.getData();
  }

  getData() {
    const defaultOptions = <Options>{
      page: 1,
      limit: 10,
      fs: null,
      sort: '-createdAt',
      status: 'all'
    };

    const options = merge({}, defaultOptions, this.getOptions);

    this.userService.getUsers('standard', options);
  }

  ngOnDestroy() {
    try {
    } catch (e) { console.log('error', e); }
  }
}
