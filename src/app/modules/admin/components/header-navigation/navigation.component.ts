import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ISlimScrollOptions } from 'ngx-slimscroll';


@Component({
  selector: 'ma-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  host: {'(document:click)': 'handleClick($event)'},
})
export class NavigationComponent implements OnInit {
  isMsgOpen: boolean;
  user: any = JSON.parse(localStorage.getItem('user'));
  slimScrollOption: ISlimScrollOptions;
  showNewLoanButton: boolean = true;
  public elementRef;

  @Input() openNavToggler: boolean;
  @Output() openNavTogglerChange: EventEmitter<boolean>;

  constructor() {
    this.isMsgOpen = false;
    this.openNavToggler = false;
    this.openNavTogglerChange = new EventEmitter<boolean>();
    this.slimScrollOption = {
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '5',
      barBorderRadius: '20',
      alwaysVisible: false,
      visibleTimeout: 1000,
      gridOpacity: '0'
    }
  }

  ngOnInit() {}

  toogleNav() {
    this.openNavToggler = !this.openNavToggler;
    this.openNavTogglerChange.emit(this.openNavToggler);
  }


  handleClick(event){
    if (event.srcElement.className === 'mdi mdi-message') {
      return false;
    }
    if (this.isMsgOpen) {
      this.isMsgOpen = false;
    }
  }

}
