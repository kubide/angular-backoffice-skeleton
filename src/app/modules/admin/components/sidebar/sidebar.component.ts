import {Component, Input} from '@angular/core';

@Component({
  selector: 'ma-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  @Input() isNavOpen: boolean = false;
  isCollapsed = {
    dashboard: true
  };

  constructor() {
  }


}
