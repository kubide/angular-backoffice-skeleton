import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  today: number = Date.now();
  openNav: boolean = false;

  constructor() {
  }
}
