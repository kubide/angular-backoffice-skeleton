import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSlimScrollModule } from 'ngx-slimscroll';

import { NavigationComponent } from './components/header-navigation/navigation.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AdminComponent } from './admin.component';
import { LocalStorageModule } from 'shared/services/localStorage/localStorage.module';
import { SillyModule } from 'shared/services/silly/silly.module';
import { AuthGuard } from 'shared/services/auth/auth.guard';
import { AuthModule } from 'shared/services/auth/auth.module';
import { AuthHttpInterceptor } from 'shared/services/auth/auth.http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ExtraTitlePipe } from 'shared/pipes/extra.title.pipe';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './modules/dashboards-list/dashboards-list.module#DashboardsListModule'
      },
      {
        path: 'users',
        loadChildren: './modules/users-list/users-list.module#UsersListModule'
      },
    ]
  }
];

@NgModule({
  declarations: [
    AdminComponent,
    NavigationComponent,
    SidebarComponent,
    ExtraTitlePipe,
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgSlimScrollModule,
    LocalStorageModule,
    SillyModule,
    AuthModule,
    NgbModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    }
  ],
})
export class AdminModule { }
