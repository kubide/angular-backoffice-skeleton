import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'shared/services/auth/auth.service';

@Component({
  selector: 'app-logout',
  template: '<div></div>',
})
export class LogoutComponent implements OnInit{
  constructor(private router: Router,
    private authService: AuthService) {
  }

  ngOnInit(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
