import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: 'upload-image.html',
  styleUrls: ['./upload-image.scss']
})

export class UploadImageComponent implements OnInit {
  @Input() images;
  @Input() onlyView: boolean;
  @Input() max: number;
  @ViewChild('uploadImage') uploadImage: any;
  private uploader: FileUploader;
  private imageUpload: any;
  private fileError: string;
  private gallery: any = [];
  private hideInput = false;

  constructor() {
    const token = JSON.parse(localStorage.getItem('tokens')).access_token;
    this.uploader = new FileUploader ({
      url: `${environment.api}/uploads/medias`,
      autoUpload: true,
      queueLimit: this.max,
      itemAlias: 'file',
      allowedMimeType : [
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/bmp',
        'image/x-windows-bmp',
        'image/gif'
      ],
      additionalParameter: {
        'profile': 'default'
      },
      authToken: `Bearer ${token}`
    });
  }

  ngOnInit() {
    // console.log('Dentro de la gallerty', this.images)
    this.max = this.max ? this.max : 1;

    this.gallery = this.images ? this.images : [];

    // check max
    if (this.gallery.length === this.max) {
      this.hideInput = true;
    }

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
      this.fileError = null;
    };

    this.uploader.onWhenAddingFileFailed = (item, filter, options) => {
      this.fileError = 'error. when adding failed';
    };

    this.uploader.onErrorItem = (item, response, status, headers) => {
      this.fileError = 'error. when upload';
    };

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      this.gallery.push(JSON.parse(response));
      this.uploader.clearQueue();
      this.reset();
    };
  }

  private reset() {
    if (this.gallery.length === this.max) {
      this.hideInput = true;
    }
    this.uploadImage.nativeElement.value = '';
  }

  private upload(event) {
    if (this.uploader.queue.length > 1) {
      this.uploader.queue[0].remove();
    }
  }

  private deleteImage(imgDelete) {
    // this.gallery = _.remove(this.gallery, (image) => {
    //   return !(image['hash'] === imgDelete.hash);
    // });
    this.gallery.forEach((image, index) => {
      if (image['hash'] === imgDelete.hash) {
        this.gallery.splice(index, 1)
      }
    });
    if (this.gallery.length < this.max) {
      this.hideInput = false;
    }
  }

  public getImages() {
    return this.gallery.reduce( (carry, imageGallery) => carry.concat(imageGallery.hash), []);
  }
}
