import {Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs/Observable';

// services
import { SillyService, Silly } from 'shared/services/silly/silly.service';

@Component({
  selector: 'app-show-errors',
  styleUrls: ['./showErrors.component.css'],
  templateUrl: './showErrors.component.html'
})

export class ShowErrorsComponent implements OnInit {
  errors$: Observable<Silly[]>;
  constructor(private sillyService: SillyService ) {}

  ngOnInit() {
    this.errors$ = this.sillyService.getObservable('errors');
  }

  closeAlert(alert: Silly) {
    this.sillyService.remove(alert.hash);
  }
}
