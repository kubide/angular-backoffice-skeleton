import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'chart',
  templateUrl: './chart.html',
  styleUrls: ['./chart.scss'],
})
export class ChartComponent implements OnInit {
  @Input() data;
  @Input() labels;
  @Input() type;
  @Input() options;
  @Input() legend;
  @Input() colors;

  chartData: number[];
  chartLabels: string[];
  chartOptions: any;
  chartType: string;
  chartLegend: boolean;
  chartColors: any;

  constructor() {

  }

  ngOnInit() {
    this.chartData = this.data;
    this.chartLabels = this.labels;
    this.chartOptions = this.options;
    this.chartType = this.type;
    this.chartLegend = this.legend;
    this.chartColors = this.colors;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
