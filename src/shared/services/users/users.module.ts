import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

// services
import { UsersService } from './users.service';
import { ErrorsModule } from 'shared/services/errors/errors.module';

@NgModule({
  imports: [
    ErrorsModule,
    CommonModule,
    HttpClientModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class UsersServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UsersServiceModule,
      providers: [
        UsersService
      ]
    };
  }
}
