import { Injectable } from '@angular/core';
import { AbstractService } from '../abstract.service';
import { User } from 'shared/interfaces/user.interface';

@Injectable()
export class UsersService extends AbstractService {
  private service_endpoint: String = '/users';

  getByHash(section, hash) {
    return super.getOne(section, `${this.service_endpoint}/${hash}`);
  }

  updateUser(section, userHash, data) {
    return super.patch(section, `${this.service_endpoint}/${userHash}`, <User>data);
  }

  getUsers(section, params = {}) {
    return super.get(section, `${this.service_endpoint}`, params);
  }

  createUser(section, user) {
    return super.post(section, this.service_endpoint, <User>user);
  }

  createAPIClient(section, clientData) {
    return super.post(section, '/clients', clientData);
  }

  getCsv(section, params = {}) {
    return super.getOne(section, `${this.service_endpoint}`, params);
  }

  searchUser(section, params = {}) {
    return super.get(section, `${this.service_endpoint}`, params);
  }
}
