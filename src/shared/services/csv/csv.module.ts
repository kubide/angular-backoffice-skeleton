import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

// services
import { CsvService } from './csv.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class CsvServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CsvServiceModule,
      providers: [
        CsvService
      ]
    };
  }
}
