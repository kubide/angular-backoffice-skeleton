import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from 'environments/environment';
import 'rxjs/add/operator/map';
import { ErrorsService, Error } from 'shared/services/errors/errors.service';
import {Injectable} from "@angular/core";

export interface Headers {
  limit?: number;
  page?: number;
  total?: number;
}

export interface Options {
  page?: number;
  limit?: number;
  sort?: string;
  search?: string;
  fs?: string;
}

const parseResponse = (response) => ({
  body: response.body,
  headers: {
    total: +response.headers.get('count'),
    page: +response.headers.get('page'),
    limit: +response.headers.get('limit')
  }
});

const setParams = (params) => {
  let httpParams = new HttpParams();

  Object.keys(params).map((value) => {
    if (params[value]) {
      httpParams = httpParams.set(value, params[value]);
    }
  });

  return httpParams;
};

@Injectable()
export abstract class AbstractService {
  protected updating: string;
  protected options: Options;
  protected endPoint: string;

  protected dataStore: {
    standard: any[],
  };

  protected dataStoreHeaders: {
    standard: Headers,
  };

  protected behaviorSubjects: {
    standard: BehaviorSubject<any[]>,
  };

  protected behaviorSubjectsHeaders: {
    standard: BehaviorSubject<Headers>
  };

  protected behaviorSubjectsUpdating: {
    standard: BehaviorSubject<boolean>
  };

  protected observable: {
    standard: Observable<any[]>,
  };

  protected observableHeaders: {
    standard: Observable<Headers>
  };

  protected observableUpdating: {
    standard: Observable<boolean>,
  };

  protected baseUrl: string;
  protected errorsSection: string;

  constructor(protected http: HttpClient,
              protected errorsService: ErrorsService) {
    this.errorsSection = 'errors';
    this.baseUrl = environment.api;
    this.options = { page: 1 };

    this.dataStore = {
      standard: <any[]>[],
    };

    this.dataStoreHeaders = {
      standard: <Headers>{},
    };

    this.behaviorSubjects = {
      standard: <BehaviorSubject<any[]>>new BehaviorSubject([]),
    };

    this.behaviorSubjectsHeaders = {
      standard: <BehaviorSubject<Headers>>new BehaviorSubject(<Headers>{}),
    };

    this.behaviorSubjectsUpdating = {
      standard: <BehaviorSubject<boolean>>new BehaviorSubject(false),
    };

    this.observable = {
      standard: this.behaviorSubjects.standard.asObservable(),
    };

    this.observableHeaders = {
      standard: this.behaviorSubjectsHeaders.standard.asObservable(),
    };

    this.observableUpdating = {
      standard: this.behaviorSubjectsUpdating.standard.asObservable()
    };
  }

  public getObservable(section = 'standard', area: 'standard'|'header'|'updating' = 'standard' ) {
    this.checkSection(section);
    let ret = this.observable[section];
    if (area === 'header') {
      ret = this.observableHeaders[section];
    } else if (area === 'updating') {
      ret = this.observableUpdating[section];
    }
    return ret;
  }

  public cleanSection(section = 'standard') {
    this.checkSection(section);

    this.dataStore[section] = <any[]>[];
    this.dataStoreHeaders[section] = <Headers>{};
    this.next(section);
    this.endUpdating(section);
  }

  protected checkSection(section = 'standard', forze = false) {
    if (forze || !this.dataStore[section]) {
      this.dataStore[section] = <any[]>[];
      this.dataStoreHeaders[section] = <Headers>{};

      this.behaviorSubjects[section] = <BehaviorSubject<any[]>>new BehaviorSubject(<any[]>[]);
      this.behaviorSubjectsHeaders[section] = <BehaviorSubject<Headers>>new BehaviorSubject(<Headers>{});
      this.behaviorSubjectsUpdating[section] = new BehaviorSubject(false);

      this.observable[section] = this.behaviorSubjects[section].asObservable();
      this.observableHeaders[section] = this.behaviorSubjectsHeaders[section].asObservable();
      this.observableUpdating[section] = this.behaviorSubjectsUpdating[section].asObservable();
    }
    return true;
  }

  protected next(section) {
    this.behaviorSubjects[section].next(
      Object.assign({}, this.dataStore)[section]
    );

    this.behaviorSubjectsHeaders[section].next(
      Object.assign({}, this.dataStoreHeaders)[section]
    );
  }

  protected startUpdating(section: string = null) {
    if (section) {
      this.behaviorSubjectsUpdating[section].next(true);
    } else {
      Object.keys(this.dataStore).forEach((section2) => {
        this.startUpdating(section2);
      });
    }
  }

  protected endUpdating(section: string = null) {
    if (section) {
      this.behaviorSubjectsUpdating[section].next(false);
    } else {
      Object.keys(this.dataStore).forEach((section2) => {
        this.endUpdating(section2);
      });
    }
  }

  public get(section, endpoint, externalOptions = {}) { // Todo:  externalOptions: Options = <Options>{}
    this.endPoint = endpoint;
    section = section ? section : 'standard' ;
    this.errorsSection = `errors.${section}`;

    const options = Object.assign({}, this.options, externalOptions);
    const params = setParams(options);

    this.checkSection(section);
    this.startUpdating(section);

    this.http.get(`${this.baseUrl}${this.endPoint}`,  {observe: 'response', params})
      .map((resp) => parseResponse(resp))
      .subscribe(resp => {
          this.dataStore[section] = resp.body;
          this.dataStoreHeaders[section] = resp.headers;
          this.next(section);
          this.endUpdating(section);
        },
        error => {
          this.errorsService.create(this.errorsSection, <Error>{ payload: error });
          this.endUpdating(section);
        });
  }

  public getOne(section, endpoint, externalOptions = {}) { // Todo:  externalOptions: Options = <Options>{}
    this.endPoint = endpoint;
    section = section ? section : 'standard' ;
    this.errorsSection = `errors.${section}`;

    const options = Object.assign({}, externalOptions);
    const params = setParams(options);

    this.checkSection(section);
    this.startUpdating(section);
    this.http.get(`${this.baseUrl}${this.endPoint}`, {observe: 'response', params})
      .map((resp) => parseResponse(resp))
      .subscribe(data => {
          data = data.body; // Todo: review with Angel
          let found = false;

          this.dataStore[section].forEach((item, index) => {
            if (item.hash === data['hash']) {
              this.dataStore[section][index] = data;
              found = true;
            }
          });

          if (!found) {
            this.dataStore[section].push(data);
          }

          this.next(section);
          this.endUpdating(section);
        },
        error => {
          this.errorsService.create(this.errorsSection, <Error>{ payload: error });
          this.endUpdating(section);
        });

  }

  public post(section, endpoint, params) {
    this.endPoint = endpoint;
    section = section ? section : 'standard' ;
    this.errorsSection = `errors.${section}`;

    this.checkSection(section);
    this.startUpdating(section);

    this.http.post(`${this.baseUrl}${this.endPoint}`, params)
      .subscribe(data => {
          this.dataStore[section].push(data);
          this.next(section);
          this.endUpdating(section);
        },
        error => {
          this.errorsService.create(this.errorsSection, <Error>{ payload: error });
          this.endUpdating(section);
        });
  }

  public patch(sectionError, endpoint, params) {
    this.endPoint = endpoint;
    this.errorsSection = sectionError ? `errors.${sectionError}` : `errors.standard`;

    this.startUpdating();
    this.http.patch(`${this.baseUrl}${this.endPoint}`, params)
      .subscribe(data => {
          Object.keys(this.dataStore).forEach((section) => {

            this.dataStore[section].forEach((t, i) => {
              if (t.hash === data['hash']) {
                this.dataStore[section][i] = data;
                this.next(section);
              }
            });

          });
          this.endUpdating();
        },
        error => {
          this.errorsService.create(this.errorsSection, <Error>{ payload: error });
          this.endUpdating();
        });

  }

  public delete(sectionError, endpoint, params) {
    this.endPoint = endpoint;
    this.errorsSection = sectionError ? `errors.${sectionError}` : `errors.standard`;

    this.startUpdating();
    this.http.delete(`${this.baseUrl}${this.endPoint}`)
      .subscribe(response => {

          Object.keys(this.dataStore).forEach((section) => {
            this.dataStore[section].forEach((t, i) => {
              if (t.hash === params.hash) {
                this.dataStore[section].splice(i, 1);
                this.next(section);
              }
            });
          });
          this.endUpdating();
        },
        error => {
          this.errorsService.create(this.errorsSection, <Error>{ payload: error });
          this.endUpdating();
        });

  }
}
