import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// services
import { LocalStorageService } from './localStorage.service';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class LocalStorageModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LocalStorageModule,
      providers: [
        LocalStorageService,
      ]
    };
  }
}
