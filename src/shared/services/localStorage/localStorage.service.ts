import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';


export interface LocalStorage {
  key: string;
  value: any;
}

@Injectable()
export class LocalStorageService {

  private dataStore: {
    standard: any,
  };
  private observables: {
    standard: BehaviorSubject<any>,
  };
  private behaviorSubjects: {
    standard: Observable<any>,
  };
  private localStorage: {
    standard: boolean
  };

  constructor() {
    this.dataStore = {
      standard: <any>{},
    };
    this.observables = {
      standard: <BehaviorSubject<any>>new BehaviorSubject(<any>{}),
    };
    this.behaviorSubjects = {
      standard: this.observables.standard.asObservable(),
    };
  }

  private checkSection(section = 'standard', forze = false) {
    if (forze || !this.dataStore[section]) {
      this.dataStore[section] = <any>{};
      this.observables[section] = <BehaviorSubject<any>>new BehaviorSubject(<any>{});
      this.behaviorSubjects[section] = this.observables[section].asObservable();
    }
    return true;
  }

  getObservable(section = 'standard') {
    this.checkSection(section);
    return this.behaviorSubjects[section];
  }

  cleanSection(section = 'standard') {
    this.checkSection(section);
    this.dataStore[section] = {};
    this.observables[section].next(Object.assign({}, this.dataStore)[section]);
  }

  load(key: string, section = 'standard') {
    this.checkSection(section);
    const data = localStorage.getItem(key);
    if (data) {
      this.dataStore[section][key] = JSON.parse(data);
      this.observables[section].next(Object.assign({}, this.dataStore)[section]);
    }
  }

  create(element: LocalStorage, section = 'standard') {
    this.checkSection(section);
    localStorage.setItem(element.key, JSON.stringify(element.value));
    this.dataStore[section][element.key] = element.value;
    this.observables[section].next(Object.assign({}, this.dataStore)[section]);
  }

  update(element: LocalStorage) {
    let exists = false;
    Object.keys(this.dataStore).forEach((section) => {

      if (this.dataStore[section][element.key]) {
        this.dataStore[section][element.key] = element.value;
        localStorage.setItem(element.key, JSON.stringify(element.value));
        this.observables[section]
          .next(Object.assign({}, this.dataStore)[section]);
      }
    });
  }

  remove(key: string) {
    Object.keys(this.dataStore).forEach((section) => {

      if (this.dataStore[section][key]) {
        delete this.dataStore[section][key];
        localStorage.removeItem(key);
        this.observables[section]
          .next(Object.assign({}, this.dataStore)[section]);
      }
    });
  }
}
