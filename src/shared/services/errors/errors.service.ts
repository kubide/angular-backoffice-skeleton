import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import { UUID } from 'angular2-uuid';


export interface Error {
  hash?: string;
  payload?: any;
  section?: string;
}

/****** Sections:
 * general
 *
 *
 */

@Injectable()
export class ErrorsService {

  private dataStore: {
    standard: Error[],
  };
  private behaviorSubjects: {
    standard: BehaviorSubject<Error[]>,
  };
  private observable: {
    standard: Observable<Error[]>,
  };


  constructor() {
    this.dataStore = {
      standard: <Error[]>[],
    };
    this.behaviorSubjects = {
      standard: <BehaviorSubject<Error[]>>new BehaviorSubject([]),
    };
    this.observable = {
      standard: this.behaviorSubjects.standard.asObservable(),
    };
  }

  private checkSection(section = 'standard', forze = false) {
    if (forze || !this.dataStore[section]) {
      this.dataStore[section] = <Error[]>[];
      this.behaviorSubjects[section] = <BehaviorSubject<Error[]>>new BehaviorSubject([]);
      this.observable[section] = this.behaviorSubjects[section].asObservable();

      const subSections = section.replace(/\.([^\.])*$/gi, '');
      if (subSections !== section) {
        this.checkSection(subSections);
      }
    }
    return true;
  }

  private checkSubsections(section) {
    if (this.dataStore[section]) {
      return section;
    }

    // if exists a subsection
    const subSections = section.replace(/\.([^\.])*$/gi, '');
    if (subSections !== section) {
      return this.checkSubsections(subSections);
    }

    return 'standard';
  }

  getObservable(section = 'standard') {
    this.checkSection(section);
    return this.observable[section];
  }

  cleanSection(section = 'standard') {
    this.checkSection(section);
    this.dataStore[section] = <Error[]>[];
    this.behaviorSubjects[section].next(Object.assign({}, this.dataStore)[section]);
  }

  create(section = 'standard', error: Error, realSection: string = null) {

    realSection = realSection || section;
    error.hash = error.hash || UUID.UUID();

    error.section = realSection;

    section = this.checkSubsections(section);

    if (!this.dataStore[section]) {
      section = 'standard';
    }

    this.dataStore[section].push(error);
    this.behaviorSubjects[section].next(Object.assign({}, this.dataStore)[section]);

    const subSections = section.replace(/\.([^\.])*$/gi, '');
    if (subSections !== section) {
      this.create(subSections, error, realSection);
    }

  }

  update(error: Error, key= 'hash') {
    Object.keys(this.dataStore).forEach((section) => {

      this.dataStore[section].forEach((t, i) => {
        if (t[key] === error[key]) { this.dataStore[section][i] = error; }
      });

      this.behaviorSubjects[section]
        .next(Object.assign({}, this.dataStore)[section]);
    });
  }

  remove(hash: string, key= 'hash') {
    Object.keys(this.dataStore).forEach((section) => {

      this.dataStore[section].forEach((t, i) => {
        if (t[key] === hash) { this.dataStore[section].splice(i, 1); }
      });

      this.behaviorSubjects[section]
        .next(Object.assign({}, this.dataStore)[section]);
    });
  }
}
