import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// services
import { ErrorsService } from './errors.service';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class ErrorsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ErrorsModule,
      providers: [
        ErrorsService,
      ]
    };
  }
}
