import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

// services
import { SillyService } from './silly.service';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class SillyModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SillyModule,
      providers: [
        SillyService,
      ]
    };
  }
}
