import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';


export interface Silly {
  hash?: string;
  payload?: any;
}


@Injectable()
export class SillyService {

  private dataStore: {
    standard: Silly[],
  };
  private observables: {
    standard: BehaviorSubject<Silly[]>,
  };
  private behaviorSubjects: {
    standard: Observable<Silly[]>,
  };


  constructor() {
    this.dataStore = {
      standard: <Silly[]>[],
    };
    this.observables = {
      standard: <BehaviorSubject<Silly[]>>new BehaviorSubject([]),
    };
    this.behaviorSubjects = {
      standard: this.observables.standard.asObservable(),
    };
  }

  private checkSection(section = '+', forze = false) {
    if (forze || !this.dataStore[section]) {
      this.dataStore[section] = <Silly[]>[];
      this.observables[section] = <BehaviorSubject<Silly[]>>new BehaviorSubject([]);
      this.behaviorSubjects[section] = this.observables[section].asObservable();

      const subSections = section.replace(/\.([^\.])*$/gi, "");
      if (subSections !== section) {
        this.checkSection(subSections);
      }
    }
    return true;
  }

  getObservable(section = 'standard') {
    this.checkSection(section);
    return this.behaviorSubjects[section];
  }

  cleanSection(section = 'standard') {
    this.checkSection(section);
    this.dataStore[section] = <Silly[]>[];
    this.observables[section].next(Object.assign({}, this.dataStore)[section]);
  }

  create(silly: Silly, section = 'standard') {
    this.checkSection(section);
    this.dataStore[section].push(silly);
    this.observables[section].next(Object.assign({}, this.dataStore)[section]);

    const subSections = section.replace(/\.([^\.])*$/gi, "");
    if (subSections !== section) {
      this.create(silly, subSections);
    }

  }

  update(silly: Silly, key='hash') {
    Object.keys(this.dataStore).forEach((section) => {

      this.dataStore[section].forEach((t, i) => {
        if (t[key] === silly[key]) { this.dataStore[section][i] = silly; }
      });

      this.observables[section]
        .next(Object.assign({}, this.dataStore)[section]);
    });
  }

  remove(hash: string, key="hash") {
    Object.keys(this.dataStore).forEach((section) => {

      this.dataStore[section].forEach((t, i) => {
        if (t[key] === hash) { this.dataStore[section].splice(i, 1); }
      });

      this.observables[section]
        .next(Object.assign({}, this.dataStore)[section]);
    });
  }
}
