import {Injectable, Injector} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Tokens } from 'shared/interfaces/tokenAndCredentials.interface';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor{
  private auth$: Observable<any>;
  private tokens: Tokens;

  constructor(private authService: AuthService) {
    this.tokens = <Tokens>null;
    this.auth$ = this.authService.getObservable('tokens');
    this.auth$.subscribe((value) => {
      this.tokens = (value.user) ? value : null;
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.tokens) {
      const date = Math.floor( Date.now() / 1000 );
      if (this.tokens.expires_in <= date ) {
        this.authService.refresh();
      }

      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.tokens.access_token}`
        }
      });
    }
    return next.handle(request);
  }
}
