import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

// services
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { LocalStorageModule } from 'shared/services/localStorage/localStorage.module';
import { IPartialConfigOptions, Ng2UiAuthModule } from 'ng2-ui-auth';
import { ClientHttpInterceptor} from './client.http.interceptor';
import { environment } from 'environments/environment';

const MyAuthConfig: IPartialConfigOptions = {
  baseUrl: environment.api,
  providers: {
    linkedin: {
      url: 'tokens',
      clientId: '78de7e94aaqxo0',
    }
  }
};


@NgModule({
  imports: [
    CommonModule,
    LocalStorageModule,
    HttpClientModule,
    Ng2UiAuthModule.forRoot(MyAuthConfig),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ClientHttpInterceptor,
      multi: true,
    }
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthGuard,
        AuthService
      ]
    };
  }
}
