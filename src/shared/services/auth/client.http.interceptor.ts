import {Injectable, Injector} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {environment} from 'environments/environment';

@Injectable()
export class ClientHttpInterceptor implements HttpInterceptor{
  private clientID: string;
  private clientSecret: string;

  constructor() {
    this.clientID = environment.clientID;
    this.clientSecret = environment.clientSecret;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Authorization: `Bearer ${this.auth.getToken()}`
    const clientCredentials = btoa(`${this.clientID}:${this.clientSecret}`);

    request = request.clone({
      setHeaders: {
        Authorization: `Basic ${clientCredentials}`
      }
    });

    return next.handle(request);
  }
}
