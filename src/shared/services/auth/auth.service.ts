import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from 'environments/environment';
import 'rxjs/add/operator/map';
import { AuthService as SocialAuthService } from 'ng2-ui-auth';


import { ErrorsService, Error } from 'shared/services/errors/errors.service';
import { User } from 'shared/interfaces/user.interface';
import { Credentials, Tokens } from 'shared/interfaces/tokenAndCredentials.interface';

export interface Headers {
  limit?: number;
  page?: number;
  total?: number;
}

@Injectable()
export class AuthService {
  public currentUser: User;
  private headers: string;

  private dataStore: {
    tokens: Tokens,
    user: User,
    headers: HttpHeaders
  };
  private observables: {
    tokens: BehaviorSubject<Tokens>,
    user: BehaviorSubject<User>,
    headers: BehaviorSubject<HttpHeaders>
  };
  private behaviorSubjects: {
    tokens: Observable<Tokens>,
    user: Observable<User>,
    headers: Observable<HttpHeaders>
  };

  private baseUrl: string;
  private errorsSection: string;
  private clientID: string;
  private clientSecret: string;

  constructor(
    private http: HttpClient,
    private socialAuth: SocialAuthService,
    private errorsService: ErrorsService) {
    this.errorsSection = 'errors.auth';
    this.headers = 'Headers';
    this.baseUrl = `${environment.api}/tokens`;

    this.dataStore = {
      tokens: <Tokens>{},
      user: <User>{},
      headers: <HttpHeaders>{}
    };
    this.observables = {
      tokens: <BehaviorSubject<Tokens>>new BehaviorSubject(<Tokens>{}),
      user: <BehaviorSubject<User>>new BehaviorSubject(<User>{}),
      headers: <BehaviorSubject<HttpHeaders>>new BehaviorSubject(<HttpHeaders>{})
    };

    this.behaviorSubjects = {
      tokens: this.observables.tokens.asObservable(),
      user: this.observables.user.asObservable(),
      headers: this.observables.headers.asObservable()
    };

  }

  getObservable(section = 'user') {
    return this.behaviorSubjects[section];
  }


  // login
  public login(credentials: Credentials) {

    this.http.post(this.baseUrl, credentials, {observe: 'response'})
      .subscribe(resp => { this.loginSuccess(resp.body, resp.headers); },
        (error) => { this.loginError(error); });
  }

  public loginIN() {
    this.socialAuth.authenticate('linkedin')
      .subscribe(resp => { this.loginSuccess(resp); },
        (error) => { this.loginError(error); });
  }

  private loginSuccess(body, headers: HttpHeaders = <HttpHeaders>{}) {
    body['expires_in'] = this.parseExpiresIn(
      body['access_token'],
      body['expires_in'] / 2);

    this.setDataStore(<Tokens>body, headers);
  }

  private loginError(error) {
    this.logout();
    const section = error.status === 401 ? this.errorsSection : 'standard';
    this.errorsService.create(section, <Error>{ payload: error });
  }


  // logout
  logout() {
    this.setDataStore();
  }

  // refresh
  refresh() {
    const clientCredentials = btoa(`${this.clientID}:${this.clientSecret}`);
    const credentials = <Credentials>{
      grant_type: 'refresh_token',
      refresh_token: this.dataStore.tokens.refresh_token,
    };

    const headers = new HttpHeaders().set('Authorization', `Basic ${clientCredentials}`);

    this.http.post(this.baseUrl, credentials, {observe: 'response', headers})
      .subscribe(resp => {
        resp.body['expires_in'] = this.parseExpiresIn(
          resp.body['access_token'],
          resp.body['expires_in'] / 2);


        this.setDataStore(<Tokens>resp.body, resp.headers);
      }, (error) => {
        this.logout();
        const section = error.status === 401 ? this.errorsSection : 'standard';
        this.errorsService.create(section, <Error>{ payload: error });
      });
  }

  // loadTokensFromStorage
  loadTokens() {
    const tokens = <Tokens>JSON.parse(localStorage.getItem('tokens'));

    if (tokens) {
      this.setDataStore(tokens);

      const date = Math.floor( Date.now() / 1000 );
      if (tokens.expires_in <= date ) {
        this.refresh();
      }
    }
  }

  private setDataStore(tokens: Tokens = <Tokens>{}, headers: HttpHeaders = <HttpHeaders>{}) {

    if (tokens.access_token && tokens.user) {
      localStorage.setItem('tokens', JSON.stringify(tokens));
      localStorage.setItem('user', JSON.stringify(tokens.user));
    } else {
      localStorage.removeItem('tokens');
      localStorage.removeItem('user');
    }

    this.dataStore.tokens = tokens;
    this.observables.tokens.next(Object.assign({}, this.dataStore).tokens);

    const user = tokens.user ? tokens.user : <User>{};
    this.dataStore.user = user;
    this.currentUser = user;

    this.observables.user.next(Object.assign({}, this.dataStore).user);
    this.dataStore.headers = headers;
    this.observables.headers.next(Object.assign({}, this.dataStore).headers);
  }

  private parseExpiresIn(accessToken, reduceTime = 0) {
    let tokens = accessToken.split('.');
    tokens = JSON.parse(atob(tokens[1]));
    return tokens.exp - reduceTime;
  }

  private setParams(params) {
    let httpParams = new HttpParams();

    Object.keys(params).map((value) => {
      httpParams = httpParams.set(value, params[value]);
    });

    return httpParams;
  }

}
