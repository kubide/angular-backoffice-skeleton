import {Injectable, OnInit} from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild{
  private auth$: Observable<any>;
  private userExists: boolean;

  constructor(
    private authService: AuthService,
    private router: Router ) {
    this.userExists = false;

    this.authService.loadTokens();

    this.authService
      .getObservable()
      .subscribe((value) => {
        this.userExists = (value.hash) ? true : false;
      });

  }

  canActivate(): boolean {
    return this.canActivateChild();
  }

  canActivateChild(): boolean {
    if (!this.userExists) {
      this.router.navigate(['/login']);
    }
    return this.userExists;
  }

}
