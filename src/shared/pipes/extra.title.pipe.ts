import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'extraTitlePipe'})
export class ExtraTitlePipe implements PipeTransform {
  transform(value: string): string {
    let param = '';
    switch (value) {
      case ('actives') : {
        param = '- Activos';
        break;
      }

      case ('pending') : {
        param = '- Pendientes';
        break;
      }

      case ('finished') : {
        param = '- Finalizados';
        break;
      }

      default: {
        param = value
      }
    }

    return param;
  }
}
