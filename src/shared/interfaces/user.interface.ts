interface Address {
  address: string;
  postalCode: string;
  city: string;
  country: string;
}
export interface User {
  // avatar?: string;
  // Internal fields
  hash?: string;
  createdAt?: string;
  updatedAt?: string;
  role?: string[];
  status?: any;
  audience?: string[];
  // Custom fields
  name: string;
  surname: string;
  birthdate: string;
  email: string;
  phone: string;
  fax: string;
  companyName: string;
  password: string;
  shippingAddress: Address;
  billingAddress: Address;
}
