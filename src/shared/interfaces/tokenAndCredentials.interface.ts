export interface Tokens {
  user: any;
  refresh_token: string;
  access_token: string;
  expires_in: number;
}

export interface Credentials {
  grant_type: 'password' | 'refresh_token' | 'linkedin';
  username?: string;
  password?: string;
  refresh_token?: string;
  data?: any;
  token_type?: 'Bearer';
}
