type Type = 'bar' | 'line';


export interface ChartJSDataset {
  type?: Type;
  label?: string;
  data: number[];
  backgroundColor?: string | string[];
  borderColor?: string;
  fill?: boolean;
}

interface axesOptions {
  ticks?: {
    min?: number;
    max?: number;
    suggestedMax?: number;
  };
}

export interface ChartJSOptions {
  responsive?: boolean;
  maintainAspectRatio?: boolean;
  legend?: {
    display: boolean;
    // filler: boolean;
  };
  scales?: {
    xAxes?: axesOptions[]
    yAxes?: axesOptions[]
  };


}



export interface ChartJS {
  type: Type;
  data: {
    labels?: string[];
    datasets: ChartJSDataset[]
  };
  options?: ChartJSOptions;
  // plugins?: filler;
}
