import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';

export class GenericAbstract {
  constructor(public _location: Location) {}

  goBack() {
    this._location.back();
  }
}
