import { Observable } from 'rxjs/Observable';
import { Options } from 'shared/services/abstract.service';
import { ToasterService } from 'angular2-toaster';
import { CsvService } from 'shared/services/csv/csv.service';

export class ListAbstract {
  Math = Math;
  dataCsv: string;
  csv$: Observable<any>;
  delayTimer: any;
  getOptions: Options = {};
  sortField: string;
  sortOrder: boolean;
  direction: any;
  csv: any;

  constructor(public csvService: CsvService,
              public toasterService: ToasterService) {}

  getData() {
    console.log('Debes implementar el metodo getData en tu controller');
  }

  newPage(event) {
    if (event) {
      this.getOptions.page = event;
      this.getData();
    }
  }
  fieldSort(field) {
    this.sortOrder = !this.sortOrder;
    this.sortField = field;

    const direction = this.sortOrder ? '' : '-';

    this.getOptions.sort = direction.concat(this.sortField);
    this.getOptions.page = 1;
    this.getData();
  }

  onSearchChange($event) {
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.getOptions.fs = $event.target.value;
      this.getOptions.page = 1;
      this.getData();
    }, 500);
  }

  exportCSV(name) {
    if (!this.dataCsv) {
      this.toasterService.pop('error', 'Error', 'Error.')
      return;
    }

    this.csvService.download(this.dataCsv, `${name}.csv`);
  }
}
