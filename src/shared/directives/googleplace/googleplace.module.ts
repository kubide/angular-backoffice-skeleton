import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleplaceDirective } from './googleplace.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
      GoogleplaceDirective
    ],
    exports: [
      GoogleplaceDirective
    ]
})

export class GoogleplaceModule {
}
